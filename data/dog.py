from dateutil.relativedelta import relativedelta
from enum import Enum
from datetime import datetime
from json import JSONEncoder
import re

_AGE_MDYY_DASH_FORMAT = '%m-%d-%Y'
_AGE_MDY_DASH_FORMAT = '%m-%d-%y'
_AGE_MDYY_SLASH_FORMAT = '%m/%d/%Y'
_AGE_MDY_SLASH_FORMAT = '%m/%d/%y'
_AGE_MDYY_DOT_FORMAT = '%m.%d.%Y'
_AGE_MDY_DOT_FORMAT = '%m.%d.%y'
_AGE_YEARS_RE = r'(?P<years>\d+)\s+year(s*)'
_AGE_MONTHS_RE = r'(?P<months>\d+)\s+month(s*)'
_AGE_WEEKS_RE = r'(?P<weeks>\d+)\s+week(s*)'
_AGE_CONTAINS_NUMBER_RE = r'\d'
_PRICE_RE = r'\$(?P<amount>[\d.]+)'

## AGES
## ====
## AGE_RE_LIST regexes expressions and match handlers are populated as the dog pages are scraped.
## EXPAND_AGE_ABBRIEVIATIONS are abbrievations for units of time and their expansions.

EXPAND_AGE_ABBRIEVIATIONS = [
    (lambda a: 'yr' in a, lambda a: a.replace('yr', 'year')),
    (lambda a: 'dov' in a, lambda a: a.replace('dov', 'dob')),
    (lambda a: '~' in a, lambda a: a.replace('~', '')),
]

AGE_RE_LIST = [
    (r'^[doborn]+\s+[approximately]+[/.]*\s*(?P<date>[\d/.-]+)$',
     lambda m: m.groupdict()['date']),
    (r'^[approximately]+[.]*\s+[dob]+\s+(?P<date>[\d/]+)$',
     lambda m: m.groupdict()['date']),
    (r'^[approximately]+\s+(?P<years>\d+).+years*$',
     lambda m: m.groupdict()['years'] + ' years'),
    (r'^[approximately]+[.]*\s+(?P<date>[\d/]+)$',
     lambda m: m.groupdict()['date']),
    (r'^[doborn]+\s+(?P<date>[\d/]+)$',
     lambda m: m.groupdict()['date']),
    (r'^(?P<months>\d+)([.-]\d+)*\s+months*\s+on\s+(?P<date>[\d/]+)$',
     lambda m: to_age(m.groupdict()['date']) - relativedelta(months=int(m.groupdict()['months']))),
    (r'^(?P<weeks>\d+)\s+weeks*\s+on\s+(?P<date>[\d/]+)$',
     lambda m: to_age(m.groupdict()['date']) - relativedelta(weeks=int(m.groupdict()['weeks']))),
    (r'^(?P<years>\d+)([.-]\d+)*\s+years*\s+(?P<months>\d+)([.-]\d+)*\s+months*$',
     lambda m: to_age(m.groupdict()['years'] + ' years') - relativedelta(months=int(m.groupdict()['months']))),
    (r'^(?P<years>\d+)([.-]\d+)*\s+years*$',
     lambda m: m.groupdict()['years'] + ' years'),
    (r'^(?P<months>\d+)([.-]\d+)*\s+months*$',
     lambda m: m.groupdict()['months'] + ' months'),
    (r'^(?P<weeks>\d+)([.-]\d+)*\s+[Ww]eeks*$',
     lambda m: m.groupdict()['weeks'] + ' weeks'),
    (r'^\d+\s+years*$',
     lambda m: m.group(0)),
    (r'^\d+\s+months*$',
     lambda m: m.group(0)),
    (r'^\d+/\d+/\d+$',
     lambda m: m.group(0)),
]

## BREEDS
## ======
## EXPAND_BREED_ABBRIEVIATIONS are breed abbrievations and their expansions.
## MIX_ABBRIEVIATIONS contains abbrievations which denote a breed is a mix.
## BREED_JUNK various strings which clutter up the breed field and can be removed.
## MULTI_WORD_BREEDS are breeds which span multiple words. These need to be enumerated to be treated
##                   as the same breed.

# tuple containing a lambda to test for the apprieviation and a lambda to expand it
EXPAND_BREED_ABBRIEVIATIONS = [
    (lambda b: 'am__bulldog' in b, lambda b: b.replace('am__bulldog', 'american_bulldog')),
    (lambda b: 'frenchie__bulldog' in b, lambda b: b.replace('frenchie__bulldog', 'french_bulldog')),
    (lambda b: 'australian__cattle__dog' in b, lambda b: b.replace('australian__cattle__dog', 'australian_cattle_dog')),
    (lambda b: 'australian__cattledog' in b, lambda b: b.replace('australian__cattledog', 'australian_cattle_dog')),
    (lambda b: 'english__cocker' in b, lambda b: b.replace('english__cocker', 'english_cocker_spaniel')),
    (lambda b: 'belgian__mal' in b, lambda b: b.replace('belgian__mal', 'belgian_malinois')),
    (lambda b: 'great__pyr' in b, lambda b: b.replace('great__pyr', 'great_pyrenees')),

    (lambda b: 'aussie' in b, lambda b: b.replace('aussie', 'australian_shepherd')),
    (lambda b: 'dobie' in b, lambda b: b.replace('dobie', 'dobermann')),
    (lambda b: 'doberman' in b, lambda b: b.replace('doberman', 'dobermann')),
    (lambda b: 'dobermannn' in b, lambda b: b.replace('dobermannn', 'dobermann')),
    (lambda b: 'gsd' in b, lambda b: b.replace('gsd', 'german_shepherd')),
    (lambda b: 'heeler' in b, lambda b: b.replace('heeler', 'australian_cattle_dog')),
    (lambda b: 'jrt' in b, lambda b: b.replace('jrt', 'jack_russell_terrier')),
    (lambda b: 'labordoodle' in b, lambda b: b.replace('labordoodle', 'labradoodle')),
    (lambda b: 'newfie' in b, lambda b: b.replace('newfie', 'newfoundland')),
    (lambda b: 'pitty' in b, lambda b: b.replace('pitty', 'pitbull')),
    (lambda b: 'puggle' in b, lambda b: b.replace('puggle', 'beagle__pug')),
    (lambda b: 'retreiver' in b, lambda b: b.replace('retreiver', 'retriever')),
    (lambda b: 'rottie' in b, lambda b: b.replace('rottie', 'rottweiler')),
    (lambda b: 'rotti' in b, lambda b: b.replace('rotti', 'rottweiler')),
    (lambda b: 'scottie' in b, lambda b: b.replace('scottie', 'scottish_terrier')),
    (lambda b: 'setterdoodle' in b, lambda b: b.replace('setterdoodle', 'irish_setter__poodle')),

    (lambda b: 'anatolian' in b and 'anatolian_shepherd' not in b,
        lambda b: b.replace('anatolian', 'anatolian_shepherd')),
    (lambda b: 'boston' in b and 'boston_terrier' not in b,
        lambda b: b.replace('boston', 'boston_terrier')),
    (lambda b: 'chesapeake' in b and 'chesapeake_retriever' not in b and 'chesapeake_bay_retriever' not in b,
        lambda b: b.replace('chesapeake', 'chesapeake_bay_retriever')),
    (lambda b: 'dane' in b and 'great_dane' not in b,
        lambda b: b.replace('dane', 'great_dane')),
    (lambda b: 'frenchie' in b and 'bulldog' not in b,
        lambda b: b.replace('frenchie', 'french_bulldog')),
    (lambda b: 'cattledog' in b and 'australian_cattle_dog' not in b,
        lambda b: b.replace('cattledog', 'australian_cattle_dog')),
    (lambda b: 'cocker' in b and 'english' not in b and 'spaniel' not in b,
        lambda b: b.replace('cocker', 'cocker_spaniel')),
    (lambda b: 'corgi' in b and 'welsh' not in b,
        lambda b: b.replace('corgi', 'welsh_corgi')),
    (lambda b: 'golden' in b and 'golden_retriever' not in b,
        lambda b: b.replace('golden', 'golden_retriever')),
    (lambda b: 'husky' in b and 'siberian husky' not in b,
        lambda b: b.replace('husky', 'siberian_husky')),
    (lambda b: 'lab' in b and 'labr' not in b and 'labo' not in b,
        lambda b: b.replace('lab', 'labrador_retriever')),
    (lambda b: 'labrador' in b and 'retriever' not in b,
        lambda b: b.replace('labrador', 'labrador_retriever')),
    (lambda b: 'shep' in b and 'shepherd' not in b,
        lambda b: b.replace('shep', 'shepherd')),
    (lambda b: 'shiba' in b and 'shiba_inu' not in b,
        lambda b: b.replace('shiba', 'shiba_inu')),
    (lambda b: 'wolfhound' in b and 'irish' not in b,
        lambda b: b.replace('wolfhound', 'irish_wolfhound')),

    (lambda b: 'chug' in b and 'chihuahua' in b and 'pug' in b,
        lambda b: b.replace('chug', '')),
]

# abbrievations denoting a breed mix
MIX_ABBRIEVIATIONS = ['__x', '__mix', '__yx', '__mox']

# strings which can be removed from the breed
BREED_JUNK = ['__does__shed']

class Breed(Enum):
    MINIATURE_AUSTRALIAN_SHEPHERD = 'miniature_australian_shepherd'
    AMERICAN_BULLDOG = 'american_bulldog'
    AMERICAN_ESKIMO = 'american_eskimo'
    AMERICAN_PITBULL = 'american_pitbull'
    ANATOLIAN_SHEPHERD = 'anatolian_shepherd'
    AUSSIE = 'aussie'
    AUSTRALIAN = 'australian'
    AUSTRALIAN_CATTLE_DOG = 'australian_cattle_dog'
    AUSTRALIAN_KELPIE = 'australian_kelpie'
    AUSTRALIAN_SHEPHERD = 'australian_shepherd'
    BASENJI = 'basenji'
    BASSET = 'basset'
    BASSET_GRIFFON_VENDEEN = 'basset_griffon_vendeen'
    BASSET_HOUND = 'basset_hound'
    BEAGLE = 'beagle'
    BELGIAN_MALINOIS = 'belgian_malinois'
    BERNESE_MOUNTAIN_DOG = 'bernese_mountain_dog'
    BLOODHOUND = 'bloodhound'
    BORDER_COLLIE = 'border_collie'
    BOSTON_TERRIER = 'boston_terrier'
    BOXER = 'boxer'
    BULLDOG = 'bulldog'
    CAIRN_TERRIER = 'cairn_terrier'
    CANE_CORSO = 'cane_corso'
    CARDIGAN_WELSH_CORGI = 'cardigan_welsh_corgi'
    CATAHOULA = 'catahoula'
    CHESAPEAKE_BAY_RETRIEVER = 'chesapeake_bay_retriever'
    CHIHUAHUA = 'chihuahua'
    CHINESE_CRESTED = 'chinese_crested'
    CHIWEENIE = 'chiweenie'
    CHOW_CHOW = 'chow_chow'
    COCKER_SPANIEL = 'cocker_spaniel'
    COLLIE = 'collie'
    COONHOUND = 'coonhound'
    CUR = 'cur'
    DACHSHUND = 'dachshund'
    DEERHOUND = 'deerhound'
    DOBERMANN = 'dobermann'
    DOBERMAN_PINSCHER = 'doberman_pinscher'
    ENGLISH_COCKER_SPANIEL = 'english_cocker_spaniel'
    ENGLISH_SETTER = 'english_setter'
    ENGLISH_BULLDOG = 'english_bulldog'
    FRENCH_BULLDOG = 'french_bulldog'
    GERMAN_SHEPHERD = 'german_shepherd'
    GOLDEN_RETRIEVER = 'golden_retriever'
    GREAT_DANE = 'great_dane'
    GREAT_PYRENEES = 'great_pyrenees'
    HAVANESE = 'havanese'
    HOUND = 'hound'
    IRISH_SETTER = 'irish_setter'
    IRISH_WOLFHOUND = 'irish_wolfhound'
    ITALIAN_GREYHOUND = 'italian_greyhound'
    JACK_RUSSELL_TERRIER = 'jack_russell_terrier'
    KLEE_KAI = 'klee_kai'
    LABRADOODLE = 'labradoodle'
    LABRADOR_RETRIEVER = 'labrador_retriever'
    MALAMUTE = 'malamute'
    MALTESE = 'maltese'
    MASTIFF = 'mastiff'
    MINIATURE_PINSCHER = 'miniature_pinscher'
    MINIATURE_POODLE = 'miniature_poodle'
    MINIATURE_SCHNAUZER = 'miniature_schnauzer'
    MINIATUR_SCHNAUZER = 'miniatur_schnauzer'
    NEWFOUNDLAND = 'newfoundland'
    PAPILLION = 'papillon'
    PATTERDALE_TERRIER = 'patterdale_terrier'
    PEKINGESE = 'pekingese'
    PITBULL = 'pitbull'
    PLOTT_HOUND = 'plott_hound'
    POINTER = 'pointer'
    POMERANIAN = 'pomeranian'
    POMSKY = 'pomsky'
    POODLE = 'poodle'
    PUG = 'pug'
    RAT_TERRIER = 'rat_terrier'
    REDBONE_COONHOUND = 'redbone_coonhound'
    RETRIEVER = 'retriever'
    RHODESIAN = 'rhodesian'
    ROTTWEILER = 'rottweiler'
    SCHNAUZER = 'schnauzer'
    SCHNOODLE = 'schnoodle'
    SCOTTISH_TERRIER = 'scottish_terrier'
    SHAR_PEI = 'shar_pei'
    SHEPHERD = 'shepherd'
    SHIBA_INU = 'shiba_inu'
    SHIH_TZU = 'shih_tzu'
    SIBERIAN_HUSKY = 'siberian_husky'
    SPANIEL = 'spaniel'
    ST_BERNARD = 'st_bernard'
    TERRIER = 'terrier'
    TIBETAN_SPANIEL = 'tibetan_spaniel'
    TIBETAN_MASTIFF = 'tibetan_mastiff'
    TOY_POODLE = 'toy_poodle'
    UNKNOWN = 'unknown'
    WEIMARANER = 'weimaraner'
    WELSH_CORGI = 'welsh_corgi'
    WHIPPET = 'whippet'
    YORKIE = 'yorkie'
    YORKIPOO = 'yorkipoo'
    YORKSHIRE_TERRIER = 'yorkshire_terrier'

## BREED constructs and the Breed enum are populated as dog pages are scraped.
MULTI_WORD_BREEDS = [breed.value for breed in Breed if '_' in breed.value]

class Gender(Enum):
    MALE = 'male'
    FEMALE = 'female'


class Dog:
    def __init__(self, name, age, gender,
                 breed, price, site,
                 descriptions, images):
        """Generic information about a Dog.
        
        name: str.
        age: datetime.
        gender: Gender.
        breed: [Breed].
        price: float.
        site: str (url).
        descriptions: [str].
        images: [str] (urls).
        """
        self.name = name
        self.age = age
        self.gender = gender
        self.breed = breed
        self.price = price
        self.site = site
        self.descriptions = descriptions
        self.images = images

    def __repr__(self):
        return 'Dog({}, {}, {}, {}, {}, {}, {}, {})'.format(
            self.name, self.age, self.gender,
            self.breed, self.price, self.site,
            self.descriptions, self.images)

class DogEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Breed):
            return obj.value
        elif isinstance(obj, Gender):
            return obj.value
        elif isinstance(obj, datetime):
            return obj.replace(microsecond=0).isoformat()
        elif isinstance(obj, Dog):
            return obj.__dict__
        else:
            return JSONEncoder.default(self, obj)

def to_age(age):
    """age: datetime (from str) = age of the dog with format:
            birthdate = '%m/%d/%y', '%m/%d/%Y',
                        '%m/%d/%y', '%m/%d/%Y',
                        '%m.%d.%y', '%m.%d.%Y'.
            age = '%w weeks', %m months', or '%y years'.
            
        raises a ValueError if str cannot be converted to an age (datetime)."""
    if isinstance(age, str):
        a = age.strip().lower()
        # if no numbers are present, return None
        m = re.match(_AGE_CONTAINS_NUMBER_RE, a)
        if not m:
            return None

        # try day-month-longyear
        try:
            return datetime.strptime(a, _AGE_MDYY_DASH_FORMAT)
        except ValueError:
            pass

        # try day-month-year
        try:
            return datetime.strptime(a, _AGE_MDY_DASH_FORMAT)
        except ValueError:
            pass

        # try day/month/longyear
        try:
            return datetime.strptime(a, _AGE_MDYY_SLASH_FORMAT)
        except ValueError:
            pass

        # try day/month/year
        try:
            return datetime.strptime(a, _AGE_MDY_SLASH_FORMAT)
        except ValueError:
            pass

        # try day.month.longyear
        try:
            return datetime.strptime(a, _AGE_MDYY_DOT_FORMAT)
        except ValueError:
            pass

        # try day.month.year
        try:
            return datetime.strptime(a, _AGE_MDY_DOT_FORMAT)
        except ValueError:
            pass

        # try years
        m = re.match(_AGE_YEARS_RE, a)
        if m and 'years' in m.groupdict():
            years = int(m.groupdict()['years'])
            return datetime.now() - relativedelta(years=years)

        # try months
        m = re.match(_AGE_MONTHS_RE, a)
        if m and 'months' in m.groupdict():
            months = int(m.groupdict()['months'])
            return datetime.now() - relativedelta(months=months)

        # try weeks
        m = re.match(_AGE_WEEKS_RE, a)
        if m and 'weeks' in m.groupdict():
            weeks = int(m.groupdict()['weeks'])
            return datetime.now() - relativedelta(weeks=weeks)

    elif isinstance(age, datetime):
        return age

    raise ValueError('Unable to convert "{}" to age'.format(age))

def to_gender(gender):
    """gender: Gender (from str) = gender of the dog.
    
       raises a ValueError if str cannot be converted to a gender."""
    if isinstance(gender, str):
        return Gender(gender.strip().lower())
    raise ValueError('Unable to convert "{}" to gender'.format(gender))

def to_breed(breed):
    """breed: Breed (from str) = breed of the dog with format:
              'breed1' or 'breed1__breed2' or 'breed1__breed_2'.

       returns the breed as a list of Breed enums. 
       raises a ValueError if str cannot be converted to a breed."""
    if isinstance(breed, str):
        try:
            return [Breed(b) for b in breed.split('__') if b]
        except ValueError as err:
            print('{}'.format(err))
            raise ValueError(err)

    raise ValueError('Unable to convert "{}" to breed'.format(breed))

def to_price(price):
    """price: float (from str) = price of the dog with format:
               '$100.50' or '$50' or '$1,000'.
               
       raises a ValueError if str cannot be converted to a price."""
    if isinstance(price, str):
        p = price.strip().lower().replace(',', '')
        m = re.match(_PRICE_RE, p)
        if m and 'amount' in m.groupdict():
            return float(m.groupdict()['amount'])
    raise ValueError('Unable to convert "{}" to price'.format(price))

def parse_breed(breed):
    """Parse a breed from a wide variety of string formats.
    Returns the breed as a string with the format: 'breed1__breed_2'
    
    Raises a ValueError if a breed cannot be parsed."""
    # save the original for logging if there's an error.
    # these are updated as each dog page is scraped.
    # format:
    # - convert the breed to each component separated by 2 underscores
    # - each breed in the mix is then sorted alphabetically
    b = breed.strip().lower()
    b = b.replace('(', '').replace(')', '')
    b = b.replace('/', ' ').replace(',', ' ').replace('.', ' ')
    b = re.sub(r'\s+', '__', b)

    # remove indications of mix (maybe add this back in at some point?)
    for m in MIX_ABBRIEVIATIONS:
        if m in b:
            b = b.replace(m, '')
            break

    # condense multi-word breeds to single underscores
    for mutli_word_breed in MULTI_WORD_BREEDS:
        expanded = mutli_word_breed.replace('_', '__')
        if expanded in b:
            b = b.replace(expanded, mutli_word_breed)

    # expand abbreviations
    for abbrev_in, abbrev_replace in EXPAND_BREED_ABBRIEVIATIONS:
        if abbrev_in(b):
            b = abbrev_replace(b)

    # clean up extra junk
    for j in BREED_JUNK:
        if j in b:
            b = b.replace(j, '')

    # sort breeds alphabetically
    b = '__'.join(sorted(b.split('__')))

    # if a breed cannot be found, mark it unknown
    if not b:
        b = 'unknown'

    try:
        return to_breed(b)
    except ValueError:
        print('breed = "{}" -> b = "{}"'.format(breed, b))
        assert False, 'Unable to find breed'

def parse_age(age):
    """Parse an age from a wide variety of string formats.
    
    Raises a ValueError if an age cannot be parsed."""
    # expand abbreviations
    a = age.strip().lower()
    for abbrev_in, abbrev_replace in EXPAND_AGE_ABBRIEVIATIONS:
        if abbrev_in(a):
            a = abbrev_replace(a)

    for i, (age_re, match_handler) in enumerate(AGE_RE_LIST):
        m = re.match(age_re, a)
        if m:
            a = match_handler(m)
            try:
                return to_age(a)
            except ValueError:
                print('age = "{}" -> a = "{}"'.format(age, a))
                print('i = {} age_re = "{}"'.format(i, AGE_RE_LIST[i][0]))
                assert False, 'Unable to find age'

    print('age = "{}" -> a = "{}"'.format(age, a))
    raise ValueError('Unable match regex with "{}"'.format(a))

def parse_price(price):
    """Parse a price from a wide variety of string formats.

    Raises a ValueError if a price cannot be parsed."""
    p = price
    if '$' not in price:
        p = '$' + price
    
    try:
        return to_price(p)
    except ValueError:
        print('price = "{}" -> p = "{}"'.format(price, p))
        assert False, 'Unable to find price'
