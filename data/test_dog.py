from datetime import datetime
from dateutil.relativedelta import relativedelta
import data.dog as dog

def test_to_age():
    e = datetime.strptime('05-20-91', dog._AGE_MDY_DASH_FORMAT)
    a = dog.to_age('05-20-91')
    assert e.year == a.year and e.month == a.month and e.day == a.day, 'Datetimes same MDY'

    now = datetime.now()
    e = now - relativedelta(months=+1)
    a = dog.to_age('1 month')
    assert e.year == a.year and e.month == a.month, 'Datetime of 1 month old'

    e = now - relativedelta(months=+2)
    a = dog.to_age('2 months')
    assert e.year == a.year and e.month == a.month, 'Datetime of 2 months old'

    e = now - relativedelta(years=+1)
    a = dog.to_age('1 year')
    assert e.year == a.year and e.year == a.year, 'Datetime of 1 year old'

    e = now - relativedelta(years=+2)
    a = dog.to_age('2 years')
    assert e.year == a.year and e.year == a.year, 'Datetime of 2 years old'

def test_to_gender():
    assert dog.Gender.MALE == dog.to_gender('male'), 'Gender is male'
    assert dog.Gender.FEMALE == dog.to_gender('female'), 'Gender is female'

def test_to_breed():
    assert [dog.Breed.SIBERIAN_HUSKY] == dog.to_breed('siberian_husky'), 'Gender is Husky'

def test_to_price():
    assert 100.0 == dog.to_price('$100'), 'Price is 100'
    assert 100.5 == dog.to_price('$100.50'), 'Price is 100.5'
    assert 1000.0 == dog.to_price('$1,000'), 'Price is 1000'