from bs4 import BeautifulSoup
import argparse

parser = argparse.ArgumentParser(description='Prettify an HTML file')
parser.add_argument('filename', type=str, help='HTML file to prettify')
args = parser.parse_args()

with open(args.filename, 'r') as f:
    html = f.read()

soup = BeautifulSoup(html, 'html.parser')

with open(args.filename, 'w') as f:
    f.write(soup.prettify())

