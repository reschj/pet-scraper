from scraper import second_hand_hounds as shh
from data.dog import Breed, Gender
from flask import Flask, Response, render_template
import argparse
import logging as log
app = Flask(__name__)

log.basicConfig(level=log.DEBUG)

@app.route('/')
def hello():
    return 'Hello World!'

@app.route('/dogs')
def dogs():
    dogs_json = shh.run()
    return Response(response=dogs_json,
                    status=200,
                    mimetype='application/json')

@app.route('/api')
def api():
    genders = sorted(gender.value for gender in Gender)
    breeds = sorted(breed.value for breed in Breed)
    return render_template('api.html',
                           breeds=breeds,
                           genders=genders)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Scrape pet adoption websites.')
    parser.add_argument('--server', action='store_true',
                        help='run the scraper as a webserver')
    args = parser.parse_args()
    if args.server:
        log.info('Running the server')
        app.run(host='0.0.0.0', port=8008)
    else:
        log.info('Running the pet scraper')
        shh.run()
        log.info('Completed')
