"""
Downloading HTML files to a local directory.

Maintains knowledge of:
- whether a download is currently being performed.
- when the last download occurred.
"""
from bs4 import BeautifulSoup
from datetime import datetime
import json
import requests
import time
import os
from . import file_metadata
from .file_metadata import FileMetadata
# from .file_metadata import FileMetadata, create_file_if_not_exists

# Metadata file for the downloader.
# States:
#   * initialized = a download has never occurred.
#   * completed = a download has been completed.
#       - last_download_time
#   * downloading = a download is in progress.
#       - download_start_time
DOWNLOAD_METADATA_FILE = 'DOWNLOAD_METADATA'

class Downloader(FileMetadata):
    def __init__(self, html_path):
        self.html_path = html_path

        metadata_path = '{}/{}'.format(html_path, DOWNLOAD_METADATA_FILE)
        FileMetadata.__init__(self, metadata_path)
        file_metadata.create_file_if_not_exists(metadata_path)

    def is_downloading(self):
        metadata = self._read_metadata()
        return metadata['state'] == 'downloading'

    def last_download_time(self):
        metadata = self._read_metadata()
        if metadata['state'] == 'downloading':
            return file_metadata.str_to_datetime(metadata['download_start_time'])
        elif metadata['state'] == 'completed':
            return file_metadata.str_to_datetime(metadata['last_download_time'])
        else:
            return None

    def delete_html_files(self):
        html_files = [name for name in os.listdir(self.html_path)
                      if 'html' in name]
        for filename in html_files:
            os.remove('{}/{}'.format(self.html_path, filename))

    def download(self, parse_urls_fn, list_url):
        if self.is_downloading():
            raise RuntimeError('A download is already in progress')

        # indicate a download is starting
        self._write_metadata({'state': 'downloading',
                              'download_start_time': datetime.now()})

        # download the list html
        r = requests.get(list_url)
        if not r.ok:
            raise RuntimeError('Unable to GET list html "{}"'.format(list_url))
        with open('{}/list.html'.format(self.html_path), 'w') as f:
            f.write(r.text)
        print('Saved {}'.format('list.html'))

        # parse the URLs from the list html
        soup = BeautifulSoup(r.content, 'html.parser')
        urls = parse_urls_fn(soup)

        # download the individual dog html files
        for i, url in enumerate(urls):
            r = requests.get(url)
            if not r.ok:
                raise RuntimeError('Unable to GET dog html "{}"'.format(url))

            filename = '{}/dog_{}.html'.format(self.html_path, i + 1)
            with open(filename, 'w') as f:
                f.write(r.text)
            print('Saved {}'.format(filename))

            # don't DDOS their servers
            time.sleep(1)

        # indicate the download completed
        self._write_metadata({'state': 'completed',
                              'last_download_time': datetime.now()})
