"""
Defines functions for working with, and an object which stores its state, metadata, in a file.
"""
from abc import ABC
from datetime import datetime
from json import JSONEncoder, JSONDecoder
from dateutil.parser import parse
import os.path
import json

INITIALIZED_STATE = {'state': 'initialized'}

class FileMetadata(ABC):
    def __init__(self, metadata_path):
        self.metadata_path = metadata_path

    def _write_metadata(self, data):
        with open(self.metadata_path, 'w') as f:
            return f.write(json.dumps(data,
                                      indent=4,
                                      sort_keys=True,
                                      cls=DatetimeEncoder))

    def _read_metadata(self):
        with open(self.metadata_path, 'r') as f:
            return json.loads(f.read())

def create_file_if_not_exists(path):
    if not os.path.isfile(path):
        with open(path, 'w') as f:
            f.write(json.dumps(INITIALIZED_STATE))

def str_to_datetime(str):
    return parse(str)

class DatetimeEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.replace(microsecond=0).isoformat()
        else:
            return JSONEncoder.default(self, obj)