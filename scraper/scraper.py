"""
Scraping HTML files in a local directory.

Maintains a cache of the last scrape which is updated
when it has detected the downloader has run.
"""
from bs4 import BeautifulSoup
from datetime import datetime
import json
import os
import os.path
from data.dog import DogEncoder
from . import file_metadata
from .file_metadata import FileMetadata
from .downloader import Downloader

# Metadata file for the scraper.
# States:
#   * initialized = a scrape has never occurred.
#   * completed = a scrape has been completed.
#       - last_scrape_time
SCRAPE_METADATA_FILE = 'SCRAPE_METADATA'
SCRAPE_CACHE_FILE = 'SCRAPE_CACHE'

INITIALIZED_STATE = {'state': 'initialized'}

class Scraper(FileMetadata):
    def __init__(self, html_path):
        self.html_path = html_path
        self.cache_path = '{}/{}'.format(html_path, SCRAPE_CACHE_FILE)

        metadata_path = '{}/{}'.format(html_path, SCRAPE_METADATA_FILE)
        FileMetadata.__init__(self, metadata_path)
        file_metadata.create_file_if_not_exists(metadata_path)

    def last_scrape_time(self):
        metadata = self._read_metadata()
        if metadata['state'] == 'completed':
            return file_metadata.str_to_datetime(metadata['last_scrape_time'])
        else:
            return None

    def scrape(self, parse_fn):
        """Scrape the Dog html files using parse_fn.

        Returns the JSON of the dog objects found."""
        downloader = Downloader(self.html_path)
        last_download_time = downloader.last_download_time()
        last_scrape_time = self.last_scrape_time()
        if not last_download_time:
            raise RuntimeError('No downloads have occurred, so nothing to scrape')

        # if a download is underway or last scrape occurred sooner than the last download, use the cache
        if os.path.isfile(self.cache_path):
            if downloader.is_downloading() or (last_scrape_time and last_scrape_time > last_download_time):
                with open(self.cache_path, 'r') as f:
                    return f.read()

        # scrape all the dog html files, saving them into `dogs`
        print('Re-scraping HTML files')
        dogs = []
        dog_html_files = [name for name in os.listdir(self.html_path)
                          if 'dog_' in name and 'html' in name]
        for filename in sorted(dog_html_files):
            with open('{}/{}'.format(self.html_path, filename), 'r') as f:
                soup = BeautifulSoup(f.read(), 'html.parser')
            try:
                dogs.append(parse_fn(soup))
            except Exception as e:
                print('FAILED TO PARSE DOG: {}'.format(filename))
                raise e
        
        # save the results into cache
        dogs_json = json.dumps(dogs, cls=DogEncoder)
        with open(self.cache_path, 'w') as f:
            f.write(dogs_json)
            self._write_metadata({'state': 'completed',
                                  'last_scrape_time': datetime.now()})

        return dogs_json
