from dateutil.relativedelta import relativedelta
from datetime import datetime
import threading
import time
import logging as log
from .dog import parse_dog
from .list import parse_urls
from scraper.downloader import Downloader
from scraper.scraper import Scraper

LIST_URL = 'https://secondhandhounds.org/dogs-for-adoption/'
HTML_PATH = 'html/second_hand_hounds'

def run():
    """
    Scrape secondhandhounds and return JSON of the dogs found.

    If the last download was greater than 24hrs, the individual dog html files are downloaded.
    If the last scrape was after that last download, the resulting JSON is cached - so,
    this function may be called many times without performance overhead.
    """
    _run_downloader()
    return _run_scraper()

def _run_downloader():
    downloader = Downloader(HTML_PATH)
    if downloader.is_downloading():
        log.info('A download is in progress, let it finish')
        return
    
    if not downloader.last_download_time() or downloader.last_download_time() < (datetime.now() - relativedelta(hours=24)):
        log.info('Downloading dogs...')
        thread = threading.Thread(name='download', target=_download, args=[downloader])
        thread.start()

def _run_scraper():
    scraper = Scraper(HTML_PATH)
    return scraper.scrape(parse_dog)

def _download(downloader):
    downloader.delete_html_files()
    downloader.download(parse_urls, LIST_URL)

