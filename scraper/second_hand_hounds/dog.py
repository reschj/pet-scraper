from dateutil.relativedelta import relativedelta
from bs4 import BeautifulSoup
from collections import namedtuple
import re
import data.dog as dog

DOG_URL_FORMAT = 'http://secondhandhounds.org/dog/{}/'

# Function for parsing animal slider images
# -----------------------------------------
# <ul class="animal-slider">
#     <li>
#         <img src="https://s3.amazonaws.com/filestore.rescuegroups.org/3699/pictures/animals/5263/5263305/47860504_500x595.jpg"/>
#     </li>
#     <li>
#         <img src="https://s3.amazonaws.com/filestore.rescuegroups.org/3699/pictures/animals/5263/5263305/47860505_500x457.jpg"/>
#     </li>
# </ul>

def parse_slider_images(slider):
    return [li.img['src'] for li in slider.find_all('li')]

# Functions for parsing a description list
# ----------------------------------------
# <span>
#     Name: Oreo
#     <br/>
#     Age: DOB Approx. 4/27/12
#     <br/>
#     Gender: Female
#     <br/>
#     Breed: Pointer mix
#     <br/>
#     Weight: 65 lbs
#     <br/>
#     Dog friendly: Selective, does best with smaller dogs!
#     <br/>
#     Cat friendly: No
#     <br/>
#     Kid friendly: 16+
#     <br/>
#     House trained: Yes
#     <br/>
#     Crate trained: Yes
#     <br/>
#     Energy level: Medium
#     <br/>
#     History: Owner surrender to a shelter in KY
#     <br/>
#     Adoption Fee: $300
# </span>

DESC_LIST_PARSERS = {
    'name': lambda n: n,
    'age': dog.parse_age,
    'gender': dog.to_gender,
    'breed': dog.parse_breed,
    'weight': lambda w: w,
    'energy level': lambda e: e,
    'history': lambda h: h,
    'adoption fee': dog.parse_price,
}

DESC_LIST_KEY_MAP = {
    'Name': 'name',
    'Age': 'age',
    'Date of Birth': 'age',
    'Gender': 'gender',
    'Breed': 'breed',
    'Weight': 'weight',
    'Energy level': 'energy level',
    'History': 'history',
    'Adoption Fee': 'adoption fee',
}

# functions for finding the elements which contain the descripion list information
# these are populated as dog pages are scraped
DESC_LIST_ELES_FNS = [
    lambda soup: (s for s in soup.find_all('span')
                  if any(any(key in child for key in DESC_LIST_KEY_MAP) for child in s.children)),
    lambda soup: (p for p in soup.find_all('p')
                  if any(any(key in child for key in DESC_LIST_KEY_MAP) for child in p.children)),
]

def desc_list_eles(desc):
    """Return the DOM elements which contains the desc list information."""
    eles_with_key = [ele for eles_fn in DESC_LIST_ELES_FNS
                             for eles in eles_fn(desc)
                                 for ele in eles]
    if eles_with_key:
        return dict.fromkeys(eles_with_key)

    print('Unable to find all list information in description')
    print('{}'.format(desc.prettify()))
    assert False, 'Unable to find all list information'

def parse_desc_list(desc):
    """Returns a dict with keys equal to those in DESC_LIST_PARSERS
       and their parsed values and a list of a description about the dog."""
    # construct a dict of each description list entry and its value
    eles = desc_list_eles(desc)
    splits = [e.split(':', 1) for e in eles if e.name is None and ':' in e]
    text_dict = {key.strip(): value.strip() for key, value in splits
                 if key.strip() in DESC_LIST_KEY_MAP}

    # pass the list entries into their appropriate parsers
    ret = {DESC_LIST_KEY_MAP[key]: DESC_LIST_PARSERS[DESC_LIST_KEY_MAP[key]](value)
           for (key, value) in text_dict.items()}
    
    # ensure we found all the information
    for key in DESC_LIST_PARSERS.keys():
        if not ret or key not in ret:
            print('DESC')
            print('====')
            print(desc)
            print('ELES')
            print('====')
            print(eles)
            print('SPLITS')
            print('======')
            print(splits)
        if not ret:
            return False, 'No keys found'

        if key not in ret:
            assert False, 'Missing key "{}"'.format(key)

    return ret

def parse_dog(soup):
    """Parse the dog from the soupified html and site url.
       None is returned if the dog could not be parsed."""
    desc = soup.find('div', class_='rgDescription')
    slider = soup.find('ul', class_='animal-slider')

    if not desc or not slider:
        return None

    slider_images = parse_slider_images(slider)
    desc_list = parse_desc_list(desc)

    # add the history to overall description
    desc_paras = []
    desc_paras.append(desc_list['history'])

    # find dog ID for url from slider image URL
    if not slider_images:
        return None
    dog_id = slider_images[0].rsplit('/', 2)[1]
    site = DOG_URL_FORMAT.format(dog_id)

    return dog.Dog(name=desc_list['name'],
                   age=desc_list['age'],
                   gender=desc_list['gender'],
                   breed=desc_list['breed'],
                   price=desc_list['adoption fee'],
                   site=site,
                   descriptions=desc_paras,
                   images=slider_images)
