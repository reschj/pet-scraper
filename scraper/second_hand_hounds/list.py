from bs4 import BeautifulSoup

def parse_urls(soup):
    animals = soup.find('ul', class_='animal-list')
    all_urls = [a['href'] for a in animals.find_all('a')]
    return list(dict.fromkeys(all_urls))