from datetime import datetime
from data.dog import Gender, Breed
from dateutil.relativedelta import relativedelta
from .dog import parse_dog
from .test_lib import soup_file

def test_dog_1():
    soup = soup_file('dog_1.html')
    dog = parse_dog(soup)
    assert dog.name == 'Oreo'
    assert dog.age == datetime(month=4, day=27, year=2012)
    assert dog.gender == Gender.FEMALE
    assert dog.breed == [Breed.POINTER]
    assert dog.price == 300.0
    assert dog.site == 'http://secondhandhounds.org/dog/5263305/'
    assert dog.descriptions
    assert dog.images

def test_dog_2():
    soup = soup_file('dog_2.html')
    dog = parse_dog(soup)
    assert dog.name == "Bit O' Honey"
    assert dog.age.year == (datetime.now() - relativedelta(years=6)).year
    assert dog.gender == Gender.MALE
    assert dog.breed == [Breed.POMERANIAN]
    assert dog.price == 250.0
    assert dog.site == 'http://secondhandhounds.org/dog/5610537/'
    assert dog.descriptions
    assert dog.images

def test_dog_3():
    soup = soup_file('dog_3.html')
    dog = parse_dog(soup)
    assert dog.name == "Capelli"
    assert dog.age == datetime(month=3, day=26, year=2014)
    assert dog.gender == Gender.FEMALE
    assert dog.breed == [Breed.JACK_RUSSELL_TERRIER, Breed.PITBULL]
    assert dog.price == 250.0
    assert dog.site == 'http://secondhandhounds.org/dog/7944498/'
    assert dog.descriptions
    assert dog.images

def test_dog_4():
    soup = soup_file('dog_4.html')
    dog = parse_dog(soup)
    assert dog.name == "Atticus"
    assert dog.age.year == (datetime.now() - relativedelta(years=6)).year
    assert dog.gender == Gender.MALE
    assert dog.breed == [Breed.MINIATURE_SCHNAUZER, Breed.TERRIER]
    assert dog.price == 250.0
    assert dog.site == 'http://secondhandhounds.org/dog/8329985/'
    assert dog.descriptions
    assert dog.images
    

def test_all_dogs():
    for i in range(1, 189):
        filename = 'dog_{}.html'.format(i)
        soup = soup_file(filename)
        try:
            parse_dog(soup)
        except Exception as e:
            print('FAILED TO PARSE DOG: {}'.format(filename))
            raise e
