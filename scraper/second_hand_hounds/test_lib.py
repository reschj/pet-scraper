from bs4 import BeautifulSoup

HTML_PATH = 'html/second_hand_hounds/test'

def soup_file(filename):
    with open('{}/{}'.format(HTML_PATH, filename), 'r') as f:
        return BeautifulSoup(f.read(), 'html.parser')


if __name__ == '__main__':
    from time import sleep
    import requests
    from list import parse_urls

    # save the individual dog html files with urls extracted
    # from list.html
    with open('../../{}/{}'.format(HTML_PATH, 'list.html'), 'r') as f:
        soup = BeautifulSoup(f.read(), 'html.parser')
    urls = parse_urls(soup)

    for i, url in enumerate(urls):
        r = requests.get(url)
        if r.ok:
            filename = '../../{}/dog_{}.html'.format(HTML_PATH, i + 1)
            with open(filename, 'w') as f:
                f.write(r.text)
            print('Saved {}'.format(filename))

            # don't spam their servers..
            sleep(1)
