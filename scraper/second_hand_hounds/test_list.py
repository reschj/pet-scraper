from .list import parse_urls
from .test_lib import soup_file

def test_list():
    soup = soup_file('list.html')
    urls = parse_urls(soup)
    assert urls